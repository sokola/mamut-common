package cz.petrsokola.mamutcommon.models;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractListModel;
import javax.swing.ListModel;

public class SetSelectionModel<T> {
	
	@SuppressWarnings("serial")
	private class SetCachingListModel extends AbstractListModel<T> {
				
		private T[] cache;

		@Override
		public synchronized int getSize() {
			if (cache != null) {
				return cache.length;
			} else {
				return 0;
			}
		}

		@Override
		public synchronized T getElementAt(int index) {
			if (cache != null && cache.length > index) {
				return cache[index];
			} else {
				return null;
			}
		}
		
		@SuppressWarnings("unchecked")
		public synchronized void update(Set<T> items) {
//			T[] oldCache = cache;
			cache = (T[]) items.toArray();
			if (comparator != null) {
				Arrays.sort(cache, comparator);
			}
			// TODO may not work as expected
//			if (oldCache != null) {
//				if (oldCache.length < cache.length) {
//					fireIntervalAdded(this, 0, getSize() - 1);
//				} else if (oldCache.length > cache.length) {
//					fireIntervalRemoved(this, index0, index1);
//				}
//			}
			fireContentsChanged(this, 0, getSize() - 1);
		}
		
	}
	
	private Set<T> allItems = new HashSet<>();
	private Set<T> selectedItems = new HashSet<>();
	private Set<T> unselectedItems = new HashSet<>();
	
	private SetCachingListModel allItemsModel = new SetCachingListModel();
	private SetCachingListModel selectedItemsModel = new SetCachingListModel();
	private SetCachingListModel unselectedItemsModel = new SetCachingListModel();
	
	private Comparator<? super T> comparator;
		
	public ListModel<T> getAllItemsListModel() {
		return allItemsModel;
	}
	
	public ListModel<T> getUnselectedItemsListModel() {
		return unselectedItemsModel;
	}
	
	public ListModel<T> getSelectedItemsListModel() {
		return selectedItemsModel;
	}
	
	public boolean addToSelection(T item) {
		if (!allItems.contains(item) || selectedItems.contains(item)) {
			return false;
		} else {
			selectedItems.add(item);
			unselectedItems.remove(item);
			selectedItemsModel.update(selectedItems);
			unselectedItemsModel.update(unselectedItems);
			return true;
		}
	}
	
	public boolean removeFromSelection(T item) {
		if (!allItems.contains(item) || !selectedItems.contains(item)) {
			return false;
		} else {
			selectedItems.remove(item);
			unselectedItems.add(item);
			selectedItemsModel.update(selectedItems);
			unselectedItemsModel.update(unselectedItems);
			return true;
		}
	}
	
	public void clearSelection() {
		selectedItems.clear();
		unselectedItems.addAll(allItems);
		selectedItemsModel.update(selectedItems);
		unselectedItemsModel.update(unselectedItems);
	}
	
	public void clearAvailableItems() {
		selectedItems.clear();
		unselectedItems.clear();
		allItems.clear();
		selectedItemsModel.update(selectedItems);
		unselectedItemsModel.update(unselectedItems);
		allItemsModel.update(allItems);
	}
	
	public boolean addToAvailableItems(T item) {
		boolean added = allItems.add(item);
		if (added) {		
			unselectedItems.add(item);
			allItemsModel.update(allItems);
			unselectedItemsModel.update(unselectedItems);			
		}		
		return added;
	}
	
	public boolean removeFromAvailableItems(T item) {
		boolean removed = allItems.remove(item);
		if (removed) {
			allItemsModel.update(allItems);
			if (unselectedItems.remove(item)) {
				unselectedItemsModel.update(unselectedItems);
			}
			if (selectedItems.remove(item)) {
				selectedItemsModel.update(selectedItems);
			}
		}
		return removed;
	}
	
	public SetSelectionModel(Set<T> availableItems) {
		if (availableItems != null) {
			setAvailableItems(availableItems);
		}
	}
	
	public void setAvailableItems(Set<T> availableItems) {
		allItems.clear();
		allItems.addAll(availableItems);
		unselectedItems.clear();
		unselectedItems.addAll(availableItems);
		selectedItems.clear();
		allItemsModel.update(allItems);
		unselectedItemsModel.update(unselectedItems);
		selectedItemsModel.update(selectedItems);
	}
	
	public SetSelectionModel() {
		this(null);
	}
		
	public Set<T> getAllAllItems() {
		return Collections.unmodifiableSet(allItems);
	}
	
	public Set<T> getUnselectedItems() {
		return Collections.unmodifiableSet(unselectedItems);
	}
	
	public Set<T> getSelectedItems() {
		return Collections.unmodifiableSet(selectedItems);
	}
	
	public Comparator<? super T> getComparator() {
		return comparator;
	}
	
	public void setComparator(Comparator<? super T> comparator) {
		this.comparator = comparator;
	}
	

}
