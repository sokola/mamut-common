package cz.petrsokola.mamutcommon;

public class MathUtils {
    /**
     * Bounds value between min and max.
     * 
     * @param min
     * @param value
     * @param max
     * @return If value is between min and max, returns value, if it is smaller
     *         than min, returns min, otherwise returns max.
     */
    public static <T extends Number> T bound(T min, T value, T max) {
        if (value.doubleValue() < min.doubleValue())
            return min;
        if (value.doubleValue() > max.doubleValue())
            return max;
        return value;
    }
}
